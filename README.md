My vim profile
==============

It comes with my default **vimrc** and the plugins I like to use

#### It will autloads

- pathogen.vim
- ruby_complete.vim

#### Bundles

- buftabs-vim
- ctrlp.vim
- nerdcommenter
- nerdtree
- pyflakes-vim
- snipmate
- snipmate_for_django
- taglist
- vim-nerdtree-tabs
- vim-pep8
